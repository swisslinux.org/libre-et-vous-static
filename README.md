libreetvous.ch, static copy of the website
==========================================

This is a static copy of the website of the 2016 verson of Rencontres
Hivernales du Libre.



Build
-----

With Podman:
```sh
podman build -t libreetvous.ch:latest .
```


With Docker:
```sh
docker build -t libreetvous.ch:latest .
```


RUN
---

With Podman:
```sh
podman run libreetvous.ch:latest
```

With Docker:
```sh
docker run libreetvous.ch:latest
```


Author
------

Sébastien Gendre <seb@k-7.ch>
